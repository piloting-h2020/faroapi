# FARO Scanner API

## a. Prerequisites

1. Install docker
2. Install docker-compose

## b. Build API

1. Clone repository
2. Open terminal and navigate to the project folder
3. To build and run the project use <docker-compose up --build> command or to just run the project without rebuilding it use <docker-compose up> command
